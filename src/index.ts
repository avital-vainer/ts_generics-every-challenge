//----------------------------
//      every challenge
//----------------------------
// implement the `every` function
// add types and generics as needed
const students = ["jOHn", "DAn", "ruTH", "jAnE", "beN"];

const hasN = (str: string) => str.toLowerCase().includes("n");
const min3Chars = (str: string) => str.length >= 3;

const ages = [72, 16, 22, 42, 36, 11, 52];
const over21 = (n: number) => n > 21;


const every = <T>(items: T[], cb: (arg: T) => boolean) => {
  for (const item of items) {
    if (cb(item) === false) {
      return false; // at least one of the items doesn't fulfill the condition
    }
  }
  return true;
};

const all_students_have_n = every(students, hasN);
const all_students_min_3 = every(students, min3Chars);
const all_ages_over_21 = every(ages, over21);

console.log({ all_students_have_n }); // -> false
console.log({ all_students_min_3 }); // -> true
console.log({ all_ages_over_21 }); // -> false